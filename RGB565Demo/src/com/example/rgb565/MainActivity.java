package com.example.rgb565;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.jni.bitmap.AndroidBitmapInfo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.hardware.Camera;
import android.hardware.Camera.ErrorCallback;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.SurfaceHolder.Callback;

public class MainActivity extends Activity implements SurfaceHolder.Callback,
		ErrorCallback {

	private int mWidth = 320;
	private int mHeight = 240;

	private Bitmap mBitmap = null;

	private SurfaceView mSurfaceview = null;
	private SurfaceHolder mSurfacehold = null;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mSurfaceview = (SurfaceView) findViewById(R.id.surfaceview);
		mSurfacehold = mSurfaceview.getHolder();
		mSurfacehold.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mSurfacehold.addCallback((Callback) this);

		findViewById(R.id.btnStart).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						FileInputStream fis = null;
						try {
							File f = new File(Environment
									.getExternalStorageDirectory().getPath()
									+ "/0000.rgb565");

							Log.d("ktian", "path-->"
									+ Environment.getExternalStorageDirectory()
											.getPath() + "/0000.rgb565");

							fis = new FileInputStream(f);

							byte[] buf = new byte[mWidth * mHeight * 2];

							int ret = fis.read(buf);

							Log.d("ktian", "ret -->" + ret);
							new ThreadDrawImage(buf).start();

						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								fis.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}

					}
				});
	}

	@Override
	public void onError(int error, Camera camera) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	}

	protected class ThreadDrawImage extends Thread {

		private Rect src;
		private Rect dst;
		private byte[] dataRgb;

		public ThreadDrawImage(byte[] data) {
			dataRgb = data;
		}

		@Override
		public void run() {

			int length = dataRgb.length;

			if (length <= 0) {
				return;
			} else {
				Log.d("ktian", "时间戳 -->" + System.currentTimeMillis());
				Log.d("ktian", "收到RGB size --->" + length);
			}

			if (mBitmap == null) {
				if (mWidth <= 0 || mHeight <= 0) {
					return;
				}

				src = new Rect(0, 0, mWidth, mHeight);
				dst = new Rect(0, 0, mSurfaceview.getWidth(),
						mSurfaceview.getHeight());
				if (dst.width() == 0 || dst.height() == 0) {
					return;
				}

				mBitmap = Bitmap.createBitmap(mWidth, mHeight, Config.RGB_565);
				if (mBitmap == null) {
					return;
				}
			}
			// fill bitmap
			// X264Jni.GetInstance().Render(bmp);
			// mBitmap.copyPixelsFromBuffer(src)
			AndroidBitmapInfo.fillRgb565(dataRgb, length, mBitmap);

			Canvas canvas = mSurfacehold.lockCanvas();
			if (canvas == null) {
				return;
			}
			canvas.drawBitmap(mBitmap, src, dst, null);
			mSurfacehold.unlockCanvasAndPost(canvas);
		}
	}

}

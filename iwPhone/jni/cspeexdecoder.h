#ifndef CSPEEXDECODER_H
#define CSPEEXDECODER_H

#include <stdio.h>
#include <stdlib.h>
#include "speex/speex_echo.h"
#include "speex/speex_preprocess.h"
#include "speex/speex.h"

class CSpeexDecoder
{
public:
    CSpeexDecoder();
    ~CSpeexDecoder();

    int InitSpeex();
    int UninitSpeex();
    int Decoder(char* data, int datalen, short *out);

private:

    int      m_nFrameSize;
    int      m_nSampleRate;

    SpeexBits m_dbits;
    void     *m_dec_state;

};

#endif // CSPEEXDECODER_H

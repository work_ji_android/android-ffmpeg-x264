#ifndef CTCPDOWNLOAD_H
#define CTCPDOWNLOAD_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <iostream>
#include <errno.h>

using namespace std;
typedef void (*TCPDownLoadCB) (char* data, int len);


class CTcpDownload
{
public:
    CTcpDownload();
    ~CTcpDownload();
    int OpenDownload(int uerid, TCPDownLoadCB cb,const char *ip, int port);
    int CloseDownload();
    int Download();
    int recvn(char* buf, int size);
    int sendn(char* data, int len);

private:
    int        m_nUserID;
    int        m_nSock;
    char      *m_pBufSend;
    char      *m_pBufRecv;
    char      *m_pbuf264;
    TCPDownLoadCB   m_cb;
    bool       m_bcancle;
    time_t     m_tmlast;

};

#endif // CTCPDOWNLOAD_H

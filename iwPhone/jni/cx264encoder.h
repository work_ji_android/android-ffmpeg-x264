#ifndef CX264Encoder_H
#define CX264Encoder_H


#include <stdio.h>
#include <pthread.h>
#include <iostream>

extern "C"
{
    #include "libavformat/avformat.h"
    #include "libavcodec/avcodec.h"
    #include "libavutil/avutil.h"
    #include "libavutil/rational.h"
    #include "libavutil/mathematics.h"
    #include "libswscale/swscale.h"
    #include "libavutil/opt.h"
}

typedef void (*X264EncoderCB)(unsigned char *data, int len);

class CX264Encoder
{
public:
    CX264Encoder();
    ~CX264Encoder();

    int OpenEncoder(X264EncoderCB cb, int width, int height, int fps, int bitrate, int gop);
    int CloseEncoder();
    int EncodeFrame(char *data);

private:
    AVCodec         *m_pVideoCodec;
    AVCodecContext  *m_pVideoCodecCtx;
    AVFrame         *m_pYuv420spFrame;
    AVFrame         *m_pYuv420pFrame;
    SwsContext      *m_pScxtYuv;
    int              m_nSizeYuv420p;
    uint8_t         *m_pBufYuv420p;
    int              m_nSizeH264;
    uint8_t         *m_pBufH264;

    int64_t          m_nFrameVideo;
    X264EncoderCB    m_cb;


};

#endif // CX264Encoder_H

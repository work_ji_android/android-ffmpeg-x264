#ifndef CSPEEXENCODER_H
#define CSPEEXENCODER_H
#include <stdio.h>
#include <stdlib.h>
#include "speex/speex_echo.h"
#include "speex/speex_preprocess.h"
#include "speex/speex.h"


class CSpeexEncoder
{
public:
    CSpeexEncoder();
    ~CSpeexEncoder();
    int InitSpeex();
    int UninitSpeex();
    int Encode(short *record, short *play, char *out);

private:
    int      m_nFrameSize;
    int      m_nFilterLen;
    int      m_nSampleRate;
    short    m_processed[160];

    SpeexBits m_ebits;
    void     *m_enc_state;

    SpeexEchoState        *m_pState;
    SpeexPreprocessState  *m_pPreprocessorState;

};

#endif // CSPEEXENCODER_H

#include "cspeexdecoder.h"

CSpeexDecoder::CSpeexDecoder()
{
    m_nFrameSize = 160;
    m_nSampleRate= 8000;
    m_dec_state = NULL;
}

CSpeexDecoder::~CSpeexDecoder()
{

}

int CSpeexDecoder::InitSpeex()
{
    speex_bits_init(& m_dbits);
    m_dec_state = speex_decoder_init(&speex_nb_mode);

    return 0;
}

int CSpeexDecoder::UninitSpeex()
{
    if(m_dec_state != NULL)
    {
        speex_decoder_destroy(m_dec_state);
        speex_bits_destroy(&m_dbits);
        m_dec_state = NULL;
    }
    return 0;
}

int CSpeexDecoder::Decoder(char* data, int datalen, short *out)
{
	if(data == NULL || m_dec_state == NULL )
	{
		return -1;
	}

    speex_bits_reset(&m_dbits);
    speex_bits_read_from(&m_dbits, data, datalen);
    int size = speex_decode_int(m_dec_state, &m_dbits, out);

    return size;
}

LOCAL_PATH := $(call my-dir)






include $(CLEAR_VARS)

LOCAL_MODULE := iwspeex

LOCAL_SRC_FILES :=  com_iwthings_speex_SpeexJni.cpp \
	                ctcpdownload.cpp \
	                ctcpupload.cpp \
		            cspeexdecoder.cpp \
		            cspeexencoder.cpp

LOCAL_C_INCLUDES +=$(LOCAL_PATH)/speex_build/include

LOCAL_LDLIBS += -L$(LOCAL_PATH)/speex_build/lib  \
	             -logg \
                 -lspeex  \
                 -lspeexdsp 

LOCAL_LDLIBS +=-L$(SYSROOT)/usr/lib -llog -lz -ljnigraphics  -lc -lgcc

LOCAL_LDFLAGS += -fuse-ld=bfd
LOCAL_CFLAGS += -D_cpusplus -mfloat-abi=softfp -mfpu=neon -march=armv7-a
LOCAL_ARM_MODE := arm
LOCAL_ARM_NEON := true
TARGET_ARCH_ABI := armeabi-v7a

include $(BUILD_SHARED_LIBRARY)










include $(CLEAR_VARS)

LOCAL_MODULE :=iwx264

LOCAL_SRC_FILES := com_iwthings_x264_X264Jni.cpp \
                   ctcpdownload.cpp \
                   ctcpupload.cpp \
                   cx264encoder.cpp \
                   cx264decoder.cpp
                  
LOCAL_C_INCLUDES +=$(LOCAL_PATH)/ffmpeg_build/include

LOCAL_LDLIBS += -L$(LOCAL_PATH)/ffmpeg_build/lib \
	           -lavformat \
               -lavcodec  \
               -lavutil \
               -lswscale \
               -lswresample \
               -lpostproc \
               -lavdevice \
               -lavfilter \
               -lx264 \
               -lfaac

LOCAL_LDLIBS +=-L$(SYSROOT)/usr/lib -llog -lz -ljnigraphics -lc -lgcc -ldl -lm

LOCAL_CFLAGS += -D_cpusplus -mfloat-abi=softfp -mfpu=neon -march=armv7-a
LOCAL_ARM_MODE := arm
LOCAL_ARM_NEON := true
TARGET_ARCH_ABI := armeabi-v7a

include $(BUILD_SHARED_LIBRARY)





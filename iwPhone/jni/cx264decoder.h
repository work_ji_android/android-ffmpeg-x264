#ifndef CX264Decoder_H
#define CX264Decoder_H
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>

using namespace std;

extern "C"
{
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libavutil/rational.h"
#include "libavutil/mathematics.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
#include "libswresample/swresample.h"
}

class CX264Decoder
{
public:
    CX264Decoder();
    ~CX264Decoder();

    int GetWidth();
    int GetHeight();
    int OpenDecode();
    int CheckSws();
    int CloseDecode();
    int Render(void *data, int width, int height);
    int DecodeFrame(char *data, int datalen);

private:

 //   int              m_nOutWidth;
 //   int              m_nOutHeight;
    int              m_nWidth;
    int              m_nHeight;
    unsigned char   *m_pRgbBuf;
    pthread_mutex_t  m_mutexRGB;
    AVCodec         *m_pVideoCodec;
    AVCodecContext  *m_pVideoCodecCtx;
    AVFrame         *m_pFrameYuv;
    AVFrame         *m_pFrameRgb;
    SwsContext      *m_pScxt;
};

#endif // CX264Decoder_H

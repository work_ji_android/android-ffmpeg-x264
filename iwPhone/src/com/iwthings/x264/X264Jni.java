package com.iwthings.x264;

//import android.graphics.Bitmap;

public class X264Jni {

	static X264Jni m_Instance = null;

	public static X264Jni GetInstance() {
		if (m_Instance == null) {
			m_Instance = new X264Jni();
		}
		return m_Instance;
	}

	static {
		System.loadLibrary("iwx264");
	}

	// 打开x264编码，编码后传送至服务器
	native public int OpenX264Encoder(int userid, String ip, int port,
			int width, int height, int bitrate, int fps, int gop);

	native public int CloseX264Encoder();

	native public int X264Encoder(byte[] data);

	// 打开x264解码，视频来自服务器
	native public int OpenX264Decode(int userid, String ip, int port);

	native public int CloseX264Decoder();

	native public int x264Decoder();

	native public int GetWidth();

	native public int GetHeight();

	native public int Render(Object bitmap);

}

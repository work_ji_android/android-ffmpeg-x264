package com.iwthings.speex;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SpeexCache 
{
	private List<short[]> m_list; 
	
	static SpeexCache Instance = null;
	
	static public  SpeexCache GetInstance()
	{
		if(Instance == null)
		{
			Instance = new SpeexCache();
			Instance.InitCache();
		}
		return Instance;
	}
	
	public int InitCache()
	{
		m_list = Collections.synchronizedList(new LinkedList<short[]>());
		return 0;
	}
	
	public int AddCache(short[] data, int size)
	{
		short[] buf = new short[size];
		System.arraycopy(data, 0, buf, 0, size);
		m_list.add(buf);
		
		return 0;
	}
	
	public short[] GetCacheAt(int index)
	{
		short[] buf = null;
		if(!m_list.isEmpty())
		{
			buf = (short[])m_list.get(index);
			
		}	
		return buf;
	}
	
	public int RemoveAt(int index)
	{
		m_list.remove(index);
		return 0;
	}
	public int RemoveAll()
	{
		m_list.clear();
		return 0;
	}
	public int GetSize()
	{
		return m_list.size();
	}

}

package com.iwthings.speex;



public class SpeexJni 
{
	
   static SpeexJni m_Instance = null;
	
	public static SpeexJni GetInstance()
	{
		if(m_Instance == null)
		{
			m_Instance = new SpeexJni();
		}
		return m_Instance;
	}	
	
	
	static
	{
		System.loadLibrary("iwspeex");
	}

	native public int OpenSpeexEncoder(int userid, String ip, int port);
	native public int CloseSpeexEncoder();
	native public int SpeexEncode(short[] record,short[] play);

	native public int OpenSpeexDecoder(int userid, String ip, int port);
	native public int CloseSpeexDecoder();
	native public int SpeexDecoder(); 

}

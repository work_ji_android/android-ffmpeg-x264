package com.iwthings.iwphone;

import java.util.Timer;
import java.util.TimerTask;
import com.iwthings.iwphone.R;
import com.iwthings.speex.SpeexPlayer;
import com.iwthings.speex.SpeexRecoder;
import com.iwthings.x264.X264Player;
import com.iwthings.x264.X264Recoder;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {

	String strUrl = "homelive.sinhonet.cn";// "124.202.151.53"
	// String strUrl = "192.168.1.116";
	int termport = 12580;
	int clientport = 12581;
	int offset = 1000000000;

	int vwidth = 320;
	int vheight = 240;

	int vbitrate = 100000;
	int vgop = 30;
	int vframe = 10;

	X264Player m_videoX264Player = null;
	X264Recoder m_videoX264Recoder = null;

	SpeexPlayer mPlayerSpeex = null;
	SpeexRecoder mRecoderSpeex = null;

	SurfaceView m_prevewview = null;
	SurfaceView m_playview = null;

	Button m_btnStop = null;
	Button m_btnStart = null;
	Button m_btnCall = null;
	EditText m_edHost = null;
	EditText m_edPeer = null;
	EditText m_edStatusStart = null;
	EditText m_edStatusCall = null;

	Handler myHandler = null;
	static final int MSG_UPLOAD = 0;
	static final int MSG_DOWNLOAD = 1;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		m_prevewview = (SurfaceView) findViewById(R.id.surfaceView1);
		m_playview = (SurfaceView) findViewById(R.id.surfaceView2);

		m_btnStop = (Button) findViewById(R.id.btnstop);
		m_btnStart = (Button) findViewById(R.id.btnstart);
		m_btnCall = (Button) findViewById(R.id.btncall);
		m_edHost = (EditText) findViewById(R.id.edhost);
		m_edPeer = (EditText) findViewById(R.id.edpeer);
		m_edStatusStart = (EditText) findViewById(R.id.edststusstart);
		m_edStatusCall = (EditText) findViewById(R.id.edstatuscall);

		m_edHost.setText("3344");
		m_edPeer.setText("3344");

		myHandler = new Handler() {
			public void handleMessage(Message msg) {
				String infoStr = msg.getData().getString("infoStr");
				if (msg.what == MSG_UPLOAD) {
					m_edStatusStart.setText(infoStr);
				} else if (msg.what == MSG_DOWNLOAD) {
					m_edStatusCall.setText(infoStr);
				}
				super.handleMessage(msg);
			}
		};

		m_btnStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final int idhost = Integer.parseInt(m_edHost.getText()
						.toString());

				Timer tm = new Timer();
				TimerTask task = new TimerTask() {
					
					@Override
					public void run() {
						int iret = 0;
						String strinfo = "";

						// ffmpeg 软编接口
						if (m_videoX264Recoder != null) {
							m_videoX264Recoder.CloseRecoder();
							m_videoX264Recoder = null;
						}

						m_videoX264Recoder = new X264Recoder();
						iret = m_videoX264Recoder.OpenRecoder(idhost, strUrl,
								termport, vwidth, vheight, vframe, vbitrate,
								m_prevewview);
						if (iret < 0) {
							strinfo += "视频失败 ";
						}

						// speex 音频接口
						if (mRecoderSpeex != null) {
							mRecoderSpeex.CloseRecoder();
							mRecoderSpeex = null;
						}

						mRecoderSpeex = new SpeexRecoder();
						iret = mRecoderSpeex.OpenRecoder(idhost + offset,
								strUrl, termport);
						if (iret < 0) {
							strinfo += "音频失败";
						}

						if (strinfo.isEmpty()) {
							strinfo = "上传成功";
						}
						// speex 结束

						Bundle infoBundle = new Bundle();
						infoBundle.putString("infoStr", strinfo);
						Message msgInfo = new Message();
						msgInfo.what = MSG_UPLOAD;
						msgInfo.setData(infoBundle);
						myHandler.sendMessage(msgInfo);

					}
				};
				
				tm.schedule(task, 500);
			}
		});

		m_btnCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				final int idpeer = Integer.parseInt(m_edPeer.getText()
						.toString());

				Timer tm = new Timer();
				TimerTask task = new TimerTask() {
					public void run() {
						String strinfo = "";
						// 视频 软解接口
						if (m_videoX264Player != null) {
							m_videoX264Player.CloseDecoder();
							m_videoX264Player = null;
						}

						m_videoX264Player = new X264Player();
						int iret = m_videoX264Player.OpenDecoder(idpeer,
								strUrl, clientport, m_playview);
						if (iret < 0) {
							strinfo += "视频失败 ";
						}

						// speex 音频接口
						if (mPlayerSpeex != null) {
							mPlayerSpeex.CloseAudioTrack();
							mPlayerSpeex = null;
						}
						mPlayerSpeex = new SpeexPlayer();
						iret = mPlayerSpeex.OpenAudioTrack(idpeer + offset,
								strUrl, clientport);
						if (iret < 0) {
							strinfo += "音频失败 ";
						}

						if (strinfo.isEmpty()) {
							strinfo = "呼叫成功";
						}

						Bundle infoBundle = new Bundle();
						infoBundle.putString("infoStr", strinfo);
						Message msgInfo = new Message();
						msgInfo.what = MSG_DOWNLOAD;
						msgInfo.setData(infoBundle);
						myHandler.sendMessage(msgInfo);
					}
				};
				tm.schedule(task, 500);
			}
		});

		m_btnStop.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Timer tm = new Timer();
				TimerTask task = new TimerTask() {
					@Override
					public void run() {
						if (m_videoX264Player != null) {
							m_videoX264Player.CloseDecoder();
							m_videoX264Player = null;
						}
						if (m_videoX264Recoder != null) {
							m_videoX264Recoder.CloseRecoder();
							m_videoX264Recoder = null;
						}

						if (mPlayerSpeex != null) {
							mPlayerSpeex.CloseAudioTrack();
							mPlayerSpeex = null;
						}
						if (mRecoderSpeex != null) {
							mRecoderSpeex.CloseRecoder();
							mRecoderSpeex = null;
						}

						String strinfo = "上传关闭";
						Bundle infoBundle = new Bundle();
						infoBundle.putString("infoStr", strinfo);
						Message msgInfo = new Message();
						msgInfo.what = MSG_UPLOAD;
						msgInfo.setData(infoBundle);
						myHandler.sendMessage(msgInfo);

						strinfo = "呼叫关闭";
						infoBundle = new Bundle();
						infoBundle.putString("infoStr", strinfo);
						msgInfo = new Message();
						msgInfo.what = MSG_DOWNLOAD;
						msgInfo.setData(infoBundle);
						myHandler.sendMessage(msgInfo);

					}

				};

				tm.schedule(task, 500);

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			System.exit(0);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		System.exit(0);
		super.onDestroy();
	}

}
